items = input("Enter the items: ").split(",")
items = [int(i) for i in items]
n = len(items)

def bubbleSort(items, n):
    attempt = 0
    for i in range(n - 1):
        for j in range(n - 1):
            attempt += 1
            if items[j] > items[j + 1]:
                (items[j], items[j + 1]) = (items[j + 1], items[j])
    return [items, attempt]

[items, attempts] = bubbleSort(items, n)
print("index: "+str(items)+"\nattempts: "+str(attempts))
