items = input("Enter the items: ").split(",")
items = [int(i) for i in items]
n = len(items)

def selectionSort(items, n):
    attempt = 0
    for i in range(n - 1):
        m = i
        for j in range(i + 1, n):
            if items[m] > items[j]:
                m = j
        if m != i:
            (items[m], items[i]) = (items[i], items[m])
        attempt += 1
    return [items, attempt]

[items, attempts] = selectionSort(items, n)
print("index: "+str(items)+"\nattempts: "+str(attempts))
