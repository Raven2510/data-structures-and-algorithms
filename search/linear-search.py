items = input("Enter the items: ").split(",")
items = [int(i) for i in items]
target = int(input("Enter the target item: "))
n = len(items)

#loop implementation
def linearSearchLoop(items, target):
    attempt = 0
    for item in items:
        attempt += 1
        if item == target:
            return [items.index(item), attempt]
    return [None, attempt]


#recursive implementation
def linearSearchRecursion(items, target, n, i = 0, attempt = 0):
    if items[i] == items[n] and items[i] != target:
        attempt += 1
        return [None, attempt]
    elif items[i] == target:
        attempt += 1
        return [i, attempt]
    else:
        return linearSearchRecursion(items, target, n, i + 1, attempt + 1)

[indexLoop, attemptsLoop] = linearSearchLoop(items, target)
[indexRecursive, attemptsRecursive] = linearSearchRecursion(items, target, n - 1)

print("(Loop) index: "+str(indexLoop)+"\nattempts: "+str(attemptsLoop))
print("\n(Recursive) index: "+str(indexRecursive)+"\nattempts: "+str(attemptsRecursive))
