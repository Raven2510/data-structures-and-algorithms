items = input("Enter the items: ").split(",")
items = [int(i) for i in items]
target = int(input("Enter the target item: "))
n = len(items)


def binarySearch(items, target, n):
    start = 0
    end = n - 1
    attempt = 0

    while start <= end:
        mid = round((start + end) / 2)

        if items[mid] > target:
            end = mid - 1
            attempt += 1
        elif items[mid] < target:
            start = mid + 1
            attempt += 1
        else:
            return [items.index(items[mid]), attempt + 1]
    return [None, attempt + 1]

[index, attempts] = binarySearch(items, target, n)

print("index: "+str(index)+"\nattempts: "+str(attempts))
